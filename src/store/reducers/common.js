import { common } from '../types';

const initialState = {
	loading: true,
};

export default ( state = initialState, { type, payload } ) => {
	switch ( type ) {
		case common.LOAD_DATA: {
			return { ...state, loading: false };
		}
		default :
			return state;
	}
}
