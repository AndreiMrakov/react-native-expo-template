import React from 'react';
// import PropTypes from 'prop-types';
import { View } from 'react-native';

import AppNavigation from './navigation';

import styles from './styles';

const Application = props => {
	return (
		<View style = { styles.container }>
			<AppNavigation />
		</View>
	);
};

Application.propTypes = {};

export default Application;
