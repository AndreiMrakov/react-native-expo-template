import React from 'react';
// import PropTypes from 'prop-types';
import { Text, View } from 'react-native';

import styles from './styles';

const HomeScreen = props => {
	return (
		<View style = { styles.container }>
			<Text style = { styles.text }>Home</Text>
		</View>
	);
};

HomeScreen.propTypes = {};

export default HomeScreen;
