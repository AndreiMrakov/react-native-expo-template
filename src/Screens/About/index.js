import React from 'react';
// import PropTypes from 'prop-types';
import { Text, View } from 'react-native';

import styles from './styles';

const AboutScreen = props => {
	return (
		<View style = { styles.container }>
			<Text style = { styles.text }>About</Text>
		</View>
	);
};

AboutScreen.propTypes = {};

export default AboutScreen;
