import HomeScreen from './Home';
import AboutScreen from './About';

export {
	HomeScreen,
	AboutScreen
}
