import { NavigationContainer } from '@react-navigation/native';
import React from 'react';
import DrawerNavigator from './DrawerNavigator';
// import PropTypes from 'prop-types';

const AppNavigation = props => {
	return (
		<NavigationContainer>
			<DrawerNavigator />
		</NavigationContainer>
	);
};

AppNavigation.propTypes = {};

export default AppNavigation;
