import { createDrawerNavigator } from '@react-navigation/drawer';
import React from 'react';
import { AboutScreen } from '../../Screens';
import TabNavigator from '../TabNavigator';
// import PropTypes from 'prop-types';

const DrawerNavigator = props => {
	const Drawer = createDrawerNavigator();
	return (
		<Drawer.Navigator>
			<Drawer.Screen name = { 'Home' } component = { TabNavigator } />
			<Drawer.Screen name = { 'About' } component = { AboutScreen } />
		</Drawer.Navigator>
	);
};

DrawerNavigator.propTypes = {};

export default DrawerNavigator;
