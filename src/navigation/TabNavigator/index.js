import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import React from 'react';
import { AboutScreen, HomeScreen } from '../../Screens';
// import PropTypes from 'prop-types';

const TabNavigator = props => {
	const Tab = createBottomTabNavigator();
	return (
		<Tab.Navigator>
			<Tab.Screen name = { 'Home' } component = { HomeScreen } />
			<Tab.Screen name = { 'About' } component = { AboutScreen } />
		</Tab.Navigator>
	);
};

TabNavigator.propTypes = {};

export default TabNavigator;
