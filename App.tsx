import React, { useState } from 'react';
import { AppLoading } from 'expo';
import { Provider } from 'react-redux';

import Store from './src/store';
import Application from './src';

export default () => {
	const [ isReady, setIsReady ] = useState( false );
	if ( !isReady ) {
		return (
			<AppLoading
				startAsync = { () => null }
				onFinish = { setIsReady.bind( null, true ) }
				onError = { console.error }
			/>
		);
	}
	return (
		<Provider store = { Store }>
			<Application />
		</Provider>
	);
}
